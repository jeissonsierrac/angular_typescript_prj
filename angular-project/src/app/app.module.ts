import Dexie from 'dexie';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, InjectionToken, Injectable, APP_INITIALIZER } from '@angular/core';
import {RouterModule, Route, Routes} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule, HttpClient, HttpHeaders, HttpRequest} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SaludadorComponent } from './components/saludador/saludador.component';
import { DestinoViajeComponent } from './components/destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './components/lista-destinos/lista-destinos.component';
import { DestinoDetalleComponent } from './components/destino-detalle/destino-detalle.component';
import { FormDestinoViajeComponent } from './components/form-destino-viaje/form-destino-viaje.component';
import {DestinoViajeState, reducerDestinoViajes, intializeDestinosViajesState, DestinosViajeEffects, InitMyDataAction} from './models/destinos-viajes-state.model'
import {ActionReducerMap, StoreFeatureModule as NgRxStoreModule, Store} from '@ngrx/store'
import {EffectsModule} from '@ngrx/effects'

import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component'
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { AuthService } from './services/auth.service';
import { VuelosComponentComponent } from './components/vuelos/vuelos-component/vuelos-component.component';
import { VuelosMainComponentComponent } from './components/vuelos/vuelos-main-component/vuelos-main-component.component';
import { VuelosMasInfoComponentComponent } from './components/vuelos/vuelos-mas-info-component/vuelos-mas-info-component.component';
import { VuelosDetalleComponentComponent } from './components/vuelos/vuelos-detalle-component/vuelos-detalle-component.component';
import { ReservasModule } from './reservas/reservas.module';
import { from, Observable } from 'rxjs';
import { promise } from 'protractor';
import { DestinoViaje } from './models/Destino-Viaje.model';
import { flatMap } from 'rxjs/operators';
import { TranslateModule } from '@ngx-translate/core';
import {NgxMapbocGLModule} from 'ngx-mapbox-gl';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { EspiameDirective } from './espiame.directive';
import { TrackearClickDirective } from './trackear-click.directive';

//injection of dependencies
export interface AppConfig
{
  apiEndpoint: String;
}

const APP_CONFIG_VALUE: AppConfig = 
{
  apiEndpoint: 'https://localhost:3000'
}

export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

export const childrenRoutesVuelos: Routes = [
  {path:'', redirectTo: 'main',pathMatch:'full'},
  {path:'main', component: VuelosMainComponentComponent},
  {path:'mas-info', component: VuelosMasInfoComponentComponent},
  {path:'id', component: VuelosDetalleComponentComponent},
]

const routes: Routes = [
  {path:'', redirectTo: 'home',pathMatch:'full'},
  {path:'home', component: ListaDestinosComponent},
  {path:'destino/:id', component: DestinoDetalleComponent},
  {path:'login', component: LoginComponent},
  {
    path:'protected',
    component: ProtectedComponent,
    canActivate:[UsuarioLogueadoGuard]
  },
  {
    path: 'vuelos',
    component: VuelosComponentComponent,
    canActivate: [UsuarioLogueadoGuard],
    children: childrenRoutesVuelos
  }  
];

//redux init
export interface AppState
{
  destinos: DestinoViajeState;
}

const reducers: ActionReducerMap<AppState> = 
{
  destinos: reducerDestinoViajes
};


const reducersInitialState = 
{
  destinos: intializeDestinosViajesState()
};

//app init
export function init_app(appLoadService: AppLoadService):() => Promise<any>
{
  return() => appLoadService.initializeDestinosViajesState();
}

//Clase AppLoadService
@Injectable()
class AppLoadService
{
  constructor(private store: Store<AppState>, private http: HttpClient){}
  async initializeDestinosViajesState():Promise<any>
  {
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN':'token-seguridad'});
    const req = new HttpRequest('GET',APP_CONFIG_VALUE.apiEndpoint+'/my',{headers:headers});
    const response: any = await this.http.request(req).toPromise();
    this.store.dispatch(new InitMyDataAction(response.body));
  }
}
//DEXIIIIIIIIIIEEEE
export class Translation
{
  constructor(public id: number, public lang: string, public key: string, public value: string){}
}
@Injectable({
  providedIn: 'root'
})

export class MyDatabase extends Dexie
{
  destinos: Dexie.Table<DestinoViaje, number>;
  translations: Dexie.Table<Translation, number>;
  constructor()
  {
    super('MyDatabase');
    //Versionado de base de datos
    this.version(1).stores({
      destinos: '++id, nombre, imagenUrl',
    });
    this.version(2).stores({
      destinos: '++id, nombre, imagenUrl',
      translations: '++id, lang, key, value'
    });
  }
}

export const db = new MyDatabase();

class TranslationLoader implements TranslationLoader
{
  constructor (private http: HttpClient){}
  getTranslation(lang: string):Observable<any>
  {
    const promise = db.translations
                    .where('lang')
                    .equals(lang)
                    .toArray()
                    .then(results =>{
                      if(results.length === 0)
                      {
                        return this.http
                        .get<Translation[]>(APP_CONFIG_VALUE.apiEndpoint+'/api/translation?lang='+lang)
                        .toPromise()
                        .then(apiResults =>{
                          return apiResults;
                        });
                      }
                      return results;
                    }).then((traducciones)=>
                    {
                      console.log('traducciones cargadas:');
                      console.log(traducciones);
                      return traducciones;
                    }).then((traducciones)=>
                    {
                      return traducciones.map((t) => ({[t.key]: t.value}));
                    });
                    return from(promise).pipe(flatMap((elems)=> from(elems)));
  }
}

function HttpLoaderFactory(http: HttpClient)
{
  return new TranslationLoader(this);
}


@NgModule({
  declarations: [
    AppComponent,
    SaludadorComponent,
    DestinoViajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent,
    NgRxStoreModule,
    LoginComponent,
    ProtectedComponent,
    VuelosComponentComponent,
    VuelosMainComponentComponent,
    VuelosMasInfoComponentComponent,
    VuelosDetalleComponentComponent,
    EspiameDirective,
    TrackearClickDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    NgRxStoreModule.ForRoot(reducers,{initialState: reducersInitialState}),
    EffectsModule.forRoot([DestinosViajeEffects]),
    StoreDevtoolsModule.instrument(),
    ReservasModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslationLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    },),
    NgxMapbocGLModule,
    BrowserAnimationsModule
  ],
  providers: [
    AuthService, UsuarioLogueadoGuard,
    {provide: APP_CONFIG, useValue: APP_CONFIG_VALUE},
    AppLoadService, 
    {provide: APP_INITIALIZER, useFactory: init_app, deps: [AppLoadService], multi: true},
    MyDatabase
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
