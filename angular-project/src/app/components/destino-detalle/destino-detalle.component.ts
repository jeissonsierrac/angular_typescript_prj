import { Component, OnInit, InjectionToken, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DestinosApiClient } from '../../models/destinos-api-client.model';
import { DestinoViaje } from '../../models/Destino-Viaje.model';


@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  providers:[
    DestinosApiClient
  ]
})
export class DestinoDetalleComponent implements OnInit {

  destino:DestinoViaje;
  style ={
    sources:{
      world: {
        type:'geoson',
        data:'https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.son'
      }
    },
    version: 8,
    layers: [{
      'id': 'countries',
      'type': 'fill',
      'source': 'world',
      'layout':{},
      'paint':{
        'fill-color':'#6f788A'
      }
    }]
  };
  constructor(private route:ActivatedRoute, private destinosPiClient:DestinosApiClient) { }

  ngOnInit(): void {
    let id = this.route.snapshot.paramMap.get('id');
    this.destino = this.destinosPiClient.getById(id); 
  }

}
