import { Component, OnInit, Input, HostBinding, Output, EventEmitter  } from '@angular/core';
import { DestinoViaje} from'./../../models/Destino-Viaje.model'
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { VoteUpAction, VoteDownAction } from '../../models/destinos-viajes-state.model';
import {trigger, state, style, transition, animate} from '@angular/animations'
@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css'],
  animations:[
    trigger('esFavorito',[
      state('estadoFavorito', style({
        backgroundColor: 'paleTurquoise'
      })),
      state('estadoNoFavorito',style({
        backgroundColor: 'WhiteSmoke'
      })),
      transition('estadoNoFavorito => estadoFavorito',
      [
        animate('3s')
      ]),
      transition('estadoFavorito => estadoNoFavorito',
      [
        animate('1s')
      ])
    ])
  ]
})
export class DestinoViajeComponent implements OnInit {


  @Input() destino: DestinoViaje;
  @Input() position: number;
  //Se agrega un atributo html al componente
  @HostBinding('attr.class')cssClass = "col-md-4";
  //Eventos
  @Output() clicked:EventEmitter<DestinoViaje>;
  constructor(private store: Store<AppState>) 
  {
    this.clicked = new EventEmitter();
  }

  ngOnInit(): void 
  {

  }

  Ir()
  {
    this.clicked.emit(this.destino);
    return false;
  }
  voteUp()
  {
    this.store.dispatch(new VoteUpAction(this.destino));
    return false
  }

  voteDown()
  {
    this.store.dispatch(new VoteDownAction(this.destino));
    return false
  }

}
