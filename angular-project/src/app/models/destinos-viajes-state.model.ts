import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { DestinoViaje } from './Destino-Viaje.model';

// Estado global de aplicacion
export interface DestinoViajeState
{
    items: DestinoViaje[];
    loading: boolean;
    favorito: DestinoViaje;
}

export function intializeDestinosViajesState()
{
    return{
        items: [],
        loading: false,
        favorito: null
    };
}
// Acciones que se pueden hacer 
export enum DestinosViajeActionTypes
{
    NUEVO_DESTINO = '[Destinos Viajes] Nuevo',
    ELEGIDO_FAVORITO = '[Destinos Viajes] Favorito',
    VOTE_UP = '[Destinos Viajes] Vote Up',
    VOTE_DOWN = '[Destinos Viajes] Vote Down',
    INIT_MY_DATA = '[Destinos Viajes] Init My Data'
}

//Clases
export class NuevoDestinoAction implements Action
{
    type = DestinosViajeActionTypes.NUEVO_DESTINO;
    constructor(public destino: DestinoViaje){}
}

export class ElegidoFavoritoAction implements Action
{
    type = DestinosViajeActionTypes.ELEGIDO_FAVORITO;
    constructor(public destino: DestinoViaje){}
}

export class VoteUpAction implements Action
{
    type = DestinosViajeActionTypes.VOTE_UP;
    constructor(public destino: DestinoViaje){}
}

export class VoteDownAction implements Action
{
    type = DestinosViajeActionTypes.VOTE_DOWN;
    constructor(public destino: DestinoViaje){}
}

export class InitMyDataAction implements Action
{
    type = DestinosViajeActionTypes.INIT_MY_DATA;
    constructor(public destinos: string[]){}
}

export type DestinosViajesActions = NuevoDestinoAction | ElegidoFavoritoAction | VoteUpAction |VoteDownAction | InitMyDataAction;

//Reducers: cada vez que se dispara una accion  se llama un reducer, recibe el estado anterior del sistema
export function reducerDestinoViajes(
    state: DestinoViajeState,
    action: DestinosViajesActions
): DestinoViajeState {
    switch (action.type)
    {
        case DestinosViajeActionTypes.NUEVO_DESTINO: 
        {
            return{ ...state,
            items:[...state.items, (action as NuevoDestinoAction).destino]};
        }

        case DestinosViajeActionTypes.ELEGIDO_FAVORITO:
        {
                state.items.forEach(x => x.setSelected(false));
                const fav: DestinoViaje = (action as ElegidoFavoritoAction).destino;
                fav.setSelected(true);
                return{ ...state,
                        favorito: fav};
        }
        case DestinosViajeActionTypes.VOTE_UP:
        {
                    const ViUp: DestinoViaje = (action as VoteUpAction).destino;
                    ViUp.VoteUp();
                    return{ ...state};
        }
        case DestinosViajeActionTypes.VOTE_DOWN:
        {
                    const ViDown: DestinoViaje = (action as VoteUpAction).destino;
                    ViDown.VoteDown();
                    return{ ...state};
        }
        case DestinosViajeActionTypes.INIT_MY_DATA:
            {
               const destinos: string[] = (action as InitMyDataAction).destinos;
               return{
                   ...state,
                   items: destinos.map((d) => new DestinoViaje(d,''))
               };
            }
    }
    return state;
}

//Effectos // Genera otra accion en efecto de otra
@Injectable()
export class DestinosViajeEffects
{
  @Effect()
  nuevoAgregado$: Observable<Action> = this.actions$.pipe
  (
      ofType(DestinosViajeActionTypes.NUEVO_DESTINO),
      map((action: NuevoDestinoAction) => new ElegidoFavoritoAction(action.destino))
  );

  constructor(private actions$: Actions){}
}