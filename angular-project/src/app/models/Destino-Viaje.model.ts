import {v4 as uuid} from 'uuid';
export class DestinoViaje
{

	private selected: boolean;
	public servicios: string[];
	id = uuid();
	constructor(public nombre: string, public url: string, public votes: number = 0)
	{
		this.servicios = ['Wifi','desayuno'];
	}

	VoteUp()
	{
        this.votes++;
	}
	
	VoteDown()
	{
		this.votes--;
	}

	isSelected(): boolean
	{
		return this.selected;
	}

	setSelected(s: boolean)
	{
		this.selected = s;
	}
}