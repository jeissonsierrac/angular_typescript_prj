import { DestinoViajeState, InitMyDataAction, reducerDestinoViajes, intializeDestinosViajesState, NuevoDestinoAction } from "./destinos-viajes-state.model"
import { DestinoViaje } from './Destino-Viaje.model';

describe('reducerDestinosViajes', () =>
{
    it('should reduce init data',() =>{
        const prevState: DestinoViajeState = intializeDestinosViajesState();
        const action: InitMyDataAction = new InitMyDataAction(['destino 1', 'destino 2']);
        const newState: DestinoViajeState = reducerDestinoViajes(prevState, action);
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].nombre).toEqual('destino 1');
    });

    it('should reduce new item added',() =>
    {
        const prevState: DestinoViajeState = intializeDestinosViajesState();
        const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('barcelona','url'));
        const newState: DestinoViajeState = reducerDestinoViajes(prevState, action);
        expect(newState.items.length).toEqual(1);
        expect(newState.items[0].nombre).toEqual('barcelona');

    })
})